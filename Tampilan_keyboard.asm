jmp start
    db  1C
    db  40
    db  C0  
    db  0

start:
    clo
    sti
    out 07

rep1:
    call    62
    nop
    jmp rep1

    org 40
    CLI    
    push    al
    push    bl
    pushf

    in  07  
    mov bl,[04]
    mov [bl],al
    inc bl
    cmp bl,0
    jnz end_char
    mov bl,c0
end_char:
    mov [04],bl

    popf
    pop bl
    pop al
    STI     
    iret

    org 62
    push    al
    pushf
    
    mov al,10
    call    91

    popf
    pop al
    ret

    org 91
    push    al
    push    bl
    pushf

    popf
    pop bl
    pop al
    ret

    end
